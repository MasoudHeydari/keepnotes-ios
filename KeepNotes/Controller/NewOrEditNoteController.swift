//
//  NewOrEditNoteController.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/17/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

protocol NewOrEditeNoteDelegate: class {
    func noteDidCreated(note: Note)
    func noteDidEdited(note: Note)
}

class NewOrEditController: UIViewController {
    
    enum Mode {
        case editNote
        case newNote
    }
    
    let mode: Mode
    weak var delegate: NewOrEditeNoteDelegate?
    
    var note: Note? {
        didSet {
            guard let note = note else { return }
            self.titleTextView.textView.text = note.note_title
            self.descTextView.textView.text = note.note_desc
        }
    }
    
    let titleTextView: MHTextView = {
        let tv = MHTextView(placeHolder: " Title", maxHeight: cSize.titleTextViewHeight)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.textView.font = UIFont.boldSystemFont(ofSize: 18)
        return tv
    }()
    
    let descTextView: MHTextView = {
        let tv = MHTextView(placeHolder: " Note", maxHeight: cSize.noteTextViewHeight)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .white
        tv.textView.font = UIFont.boldSystemFont(ofSize: 16)
        return tv
    }()
    
    public init(flag: Mode) {
        self.mode = flag
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = ( mode == .newNote ) ? Const.NavTitle.newNote : Const.NavTitle.editNote
        
        if mode == .editNote {
            // hide placeHolders
            self.titleTextView.hidePlaceHolder()
            self.descTextView.hidePlaceHolder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNavBar()
        setupNavBarButtonItems()
        // cuse navBar to has light content
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    private func setupView() {
        view.backgroundColor = .white
        addViews()
        addConstraints()
    }
    
    private func addViews() {
        view.addSubview(titleTextView)
        view.addSubview(descTextView)
    }
    
    private func addConstraints() {
        titleTextView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        titleTextView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        titleTextView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        titleTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        titleTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        descTextView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        descTextView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        descTextView.topAnchor.constraint(equalTo: titleTextView.bottomAnchor, constant: 8).isActive = true
    }
    
    
    
    private func setupNavBarButtonItems() {
        setupRightBarButtonItem()
        setupLeftBarButtonItem()
    }
    
    private func setupRightBarButtonItem() {
        let doneImg = #imageLiteral(resourceName: "done").withRenderingMode(.alwaysOriginal)
        let rightBarButtonSave = UIBarButtonItem(image: doneImg, style: .plain, target: self, action: #selector(navSaveTapped))
        navigationItem.rightBarButtonItem = rightBarButtonSave
    }
    
    private func setupLeftBarButtonItem() {
        let crossImg = #imageLiteral(resourceName: "multiply").withRenderingMode(.alwaysOriginal)
        let leftBarButtonCancel = UIBarButtonItem(image: crossImg, style: .plain, target: self, action: #selector(navCloseTapped))
        navigationItem.leftBarButtonItem = leftBarButtonCancel
        
    }
    
    @objc private func navCloseTapped() {
        dismiss(animated: true)
    }
    
    @objc private func navSaveTapped() {
        if mode == .newNote {
            saveNewNote()
        } else {
            saveEditedNote()
        }
    }
    
    private func saveNewNote() {
        guard let noteTitle = self.titleTextView.textView.text, let noteDesc = self.descTextView.textView.text else { return }
        CoreData.shared.saveNewNote(entityName: Const.DB.noteEntity, title: noteTitle, noteDesc: noteDesc) { [weak self] (newNote) in
            guard let strongSelf = self else { return }
            strongSelf.dismiss(animated: true, completion: {
                strongSelf.delegate?.noteDidCreated(note: newNote)
            })
        }
    }
    
    private func saveEditedNote() {
        guard let note = self.note, let noteTitle = self.titleTextView.textView.text, let noteDesc = self.descTextView.textView.text else { return }
        CoreData.shared.saveEditedNote(note: note, title: noteTitle, noteDesc: noteDesc) { [weak self] (editedNote) in
            guard let strongSelf = self else { return }
            strongSelf.dismiss(animated: true, completion: {
                strongSelf.delegate?.noteDidEdited(note: editedNote)
            })
        }
        
    }
}
