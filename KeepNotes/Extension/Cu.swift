//
//  Cu.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/18/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle  {
        return .lightContent
    }
}
