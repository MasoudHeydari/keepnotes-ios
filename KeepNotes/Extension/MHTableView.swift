//
//  MHTableView.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/21/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class MHTableView: UITableView {
    
    var maxHeight: CGFloat = UIScreen.main.bounds.size.height
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        let height = min(contentSize.height, maxHeight)
        return CGSize(width: contentSize.width, height: height)
    }
}
