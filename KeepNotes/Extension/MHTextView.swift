//
//  MHTextView.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/20/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class MHTextView: UIView, UITextViewDelegate {
    
    private let placeHolder: String
    private let maxHeight: CGFloat
    private let bottomPadding: CGFloat = 12
    
    private lazy var placeHolderLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = self.placeHolder
        label.textColor = .lightGray
        return label
    }()
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.font = UIFont.systemFont(ofSize: 18)
        return tv
    }()
    
    public init(placeHolder: String, maxHeight: CGFloat) {
        self.placeHolder = placeHolder
        self.maxHeight = maxHeight
        super.init(frame: .zero)
        setup()
    }
    
    override init(frame: CGRect) {
        placeHolder = ""
        maxHeight = 0
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
        translatesAutoresizingMaskIntoConstraints = false
        textView.delegate = self
        textView.isScrollEnabled = false
        autoresizingMask = .flexibleHeight
        backgroundColor = .white
        setupSubViews()
    }
    
    private func setupSubViews() {
        addSubview(textView)
        textView.addSubview(placeHolderLabel)
        
        [   textView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12),
            textView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12),
            textView.topAnchor.constraint(equalTo: topAnchor, constant: 12),
            textView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -bottomPadding),
            
            placeHolderLabel.leftAnchor.constraint(equalTo: textView.leftAnchor, constant: 4),
            placeHolderLabel.rightAnchor.constraint(equalTo: textView.rightAnchor, constant: 12),
            placeHolderLabel.topAnchor.constraint(equalTo: textView.topAnchor, constant: 12),
            placeHolderLabel.bottomAnchor.constraint(equalTo: textView.bottomAnchor, constant: -12)].forEach{$0.isActive = true }
    }
    
    public func hidePlaceHolder() {
        self.placeHolderLabel.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        return .zero
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeHolderLabel.isHidden = !textView.text.isEmpty
        
        if textView.frame.height >= (maxHeight - bottomPadding ) {
            textView.isScrollEnabled = true
            textView.heightAnchor.constraint(equalToConstant: maxHeight - bottomPadding).isActive = true
        }
    }
    
}
