//
//  UIPaddingLabel.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/17/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

@IBDesignable class UIPaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat
    @IBInspectable var bottomInset: CGFloat
    @IBInspectable var leftInset: CGFloat
    @IBInspectable var rightInset: CGFloat
    
    public init(padding: UIEdgeInsets) {
        self.topInset = padding.top
        self.rightInset = padding.right
        self.leftInset = padding.left
        self.bottomInset = padding.bottom
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}
