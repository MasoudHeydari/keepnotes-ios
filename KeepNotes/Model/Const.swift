//
//  Const.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/16/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct Const {
    static let empty = ""
    
    struct Controller {
        
        struct AllNotes {
            static let fabSize: CGFloat = 64.0
            static let noNoteToShow = "There is no note to show\n Tap on '+' button for new one"
        }
        
        struct NewOrEditNote {
            
        }
    }
    
    struct NavTitle {
        static let allNotes = "All Notes"
        static let newNote = "New Note"
        static let editNote = "Edit Note"
    }
    
    struct CellId {
        static let singleNoteCellID = "single-note-cell-id"
    }
    
    struct DB {
        
        static let name = "KeepNotesDB"
        static let noteEntity = "Note"
        
        struct Note {
            static let id = "note_id"
            static let title = "note_title"
            static let description = "note_desc"
            static let founded = "note_founded"
        }
        
        struct Notification {
            static let title = "title"
            static let date = "date"
        }
    }
}

struct cSize {
    static var titleTextViewHeight: CGFloat {
        return UIScreen.main.bounds.height / 5
    }
    
    static var noteTextViewHeight: CGFloat {
        return UIScreen.main.bounds.height - titleTextViewHeight
    }
    
    static var noteTableViewCellHeight: CGFloat {
        return UIScreen.main.bounds.height / 3
    }
    
    static var viewWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
}

