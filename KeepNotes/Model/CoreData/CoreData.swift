//
//  CoreData.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/16/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import CoreData

struct CoreData {
    static let shared = CoreData()
    
    // Singleton Design Pattern
    private init() { }
    
    let persistentContainer: NSPersistentContainer = {
        let pc = NSPersistentContainer(name: Const.DB.name)
        pc.loadPersistentStores(completionHandler: { (storeDesc, error) in
            if let error = error {
                fatalError("falied to load store: \(error)")
            }
        })
        return pc
    }()
    
    public func fetchAll(entityName: String, completion: (([Note]?, Error?) -> ())) {
        let context = persistentContainer.viewContext
        let fetchAllRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do {
            let objects = try context.fetch(fetchAllRequest) as? [Note]
            completion(objects, nil)
        } catch let fetchError {
            print("failed to get all of records, ", fetchError)
            completion(nil, fetchError)
        }
    }
    
    public func saveNewNote(entityName: String, title: String, noteDesc: String, completion: ((Note) -> ())? = nil ) {
        let context = persistentContainer.viewContext
        guard let newNote = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as? Note else { return }
        newNote.setValue(title, forKey: Const.DB.Note.title)
        newNote.setValue(noteDesc, forKey: Const.DB.Note.description)
        self.saveContext {
            completion?(newNote)
        }
    }
    
    public func saveEditedNote(note: Note, title: String, noteDesc: String, completion: ((Note) -> ())? = nil ) {
        note.note_title = title
        note.note_desc = noteDesc
        saveContext  {
            completion?(note)
        }
    }
    
    public func searchInNotes(for key: String, entityName: String, completion: ([Note]?) -> ()) {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let predicate = NSPredicate(format: "note_title CONTAINS[c] %@", key)
        fetchRequest.predicate = predicate
        
        do{
            let notes = try context.fetch(fetchRequest) as? [Note]
            completion(notes)
            
        } catch let queryError {
            print("query error for \(key) -> \(queryError)")
            completion([])
        }

    }
    
    public func delete(note: Note, completion: (() -> ())? = nil) {
        // delete the note from Core Data
        let context = persistentContainer.viewContext
        
        context.delete(note)
        
        saveContext{
            completion?()
        }
    }
    
    public func deleteAllRecords(entityName: String, completion: () -> ()) {
        let context = persistentContainer.viewContext
        let fetchRequestForDeleting = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let batchDeletRequest = NSBatchDeleteRequest(fetchRequest: fetchRequestForDeleting)
        
        do {
            try context.execute(batchDeletRequest)
        } catch let deleteBatchError {
            print("Failed to Delete all records of \(entityName) , Error -> \(deleteBatchError)")
        }
        
        // fire off the 'completion' block!
        completion()
        
    }
    
    private func saveContext(completion: (() -> ())? = nil) {
        let context = persistentContainer.viewContext
        do {
            try context.save()
            completion?()
        } catch let error {
            print("Failed to save context, Error -> ", error)
            completion?()
        }
    }
}
